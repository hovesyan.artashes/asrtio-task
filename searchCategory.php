<?php


$categories = [
    [
        "id" => 1,
        "title" =>  "Обувь",
        'children' => [
            [
                'id' => 2,
                'title' => 'Ботинки',
                'children' => [
                    ['id' => 3, 'title' => 'Кожа'],
                    ['id' => 4, 'title' => 'Текстиль'],
                ],
            ],
            ['id' => 5, 'title' => 'Кроссовки',],
        ]
    ],
    [
        "id" => 6,
        "title" =>  "Спорт",
        'children' => [
            [
                'id' => 7,
                'title' => 'Мячи'
            ]
        ]
    ],
];

function searchCategory(array $categories,int $id){
    foreach ($categories as $category){
        if($category['id'] == $id){
            return $category['title'];
        };
        if(isset($category['children'])){
            $searchChildren = searchCategory($category['children'],$id);
            if($searchChildren){
                return $searchChildren;
            }else{
                continue;
            }
        }
    }
    return false;
}

//echo searchCategory($categories,4);

