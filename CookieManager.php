<?php


class CookieManager
{
    private static $instance = null;

    private $expire_time = 0;

    private function __construct()
    {
        $this->expire_time = time() + (86400 * 365);
    }

    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new CookieManager();
        }

        return self::$instance;
    }

    public function setCookie($cookie_name, $cookie_value, $expire = null, $path = "", $domain = ""){

        if (preg_match("/[=,; \t\r\n\013\014]/", $cookie_name)) {
            throw new \InvalidArgumentException('The cookie name "' . $cookie_name . '" contains invalid characters.');
        }

        if(null == $expire){
            $expire = $this->expire_time;
        }
        else{
            $expire = $this->checkExpire($expire);
        }
        return setcookie($cookie_name, $cookie_value, $expire, $path, $domain);
    }

    public function deleteCookie($cookie_name){
        if(!isset($_COOKIE[$cookie_name])){
            throw new \InvalidArgumentException("Cookie named '" . $cookie_name . "' is not set!");
        }
        return setcookie($cookie_name, "", time() - 3600);
    }

    public function updateCookie($cookie_name, $cookie_value = null, $expire = null, $path = "", $domain = ""){
        if(!isset($_COOKIE[$cookie_name])){
            throw new \InvalidArgumentException("Cookie named '" . $cookie_name . "' is not set!");
        }
        if(null != $expire){
            $expire = $this->checkExpire($expire);
        }
        return setcookie($cookie_name, $cookie_value, $expire, $path, $domain);
    }

    public function getCookie($cookie_name){
        if(!isset($_COOKIE[$cookie_name])){
            throw new \InvalidArgumentException("Cookie named '" . $cookie_name . "' is not set!");
        }
        return $_COOKIE[$cookie_name];
    }

    private function checkExpire($expire){
        if ($expire instanceof \DateTime) {
            $expire = $expire->format('U');
            var_dump($expire);
        } elseif (!is_numeric($expire)) {
            $expire = strtotime($expire);
            var_dump($expire);
            if (false === $expire || -1 === $expire) {
                throw new \InvalidArgumentException('The cookie expiration time is not valid.');
            }
        }
        return $expire;
    }
}

//$coockieManager = CookieManager::getInstance();
