Task 1. 
file: searchCategory.php
Function searchCategory returns category title or 
false if category does not exist
searchCategory(array $categories,int $id)

Task 2. 
file: queries.php
$query1 gets the name of all departments 
in which more than 5 and workers

$query2 gets the name of all departments, 
and the ids of all workers

Task 3.
table structure is described in phone_num_database_ru.txt

Task 4. 
file: checkHtml.php

Function checkHtml get array of html tags and 
returns true if html is valid and false if not
checkHtml(array $categories,int $id)

Function checkHtmlString get string of html and 
returns true if html is valid and false if not
checkHtmlString(string $html)

Task 5.
file: CookieManager.php
CookieManager is singleton class for working with
cookies, to use this class use static 
getInstance() method