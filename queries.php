<?php
//get departments that have more then 5 workers
$query1 = "SELECT `name` FROM `worker` INNER JOIN `department`
  ON worker.department_id = department.id GROUP BY `department_id`
  HAVING COUNT(department_id) > 5;";


// get departments with their workers ids
$query2 = "SELECT `name`,GROUP_CONCAT( worker.id ) AS `total`
  FROM `department` INNER JOIN `worker` ON department.id = worker.department_id
  GROUP BY `department_id`;";